This repository contains my builds of suckless programs, packed in one diff file to patch.

## Usage:

1. Go to [suckless.org](https://suckless.org/)
2. Pick the source code of desired app
3. Copy config.def.h to config.h: `cp config.def.h config.h`
4. Apply patch to this folder: `patch -p1 < [name_of_patch]`

If the version is right, then the patch should be successful.
